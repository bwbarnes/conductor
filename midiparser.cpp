#include "midiparser.h"

MIDIParser::MIDIParser(QObject *parent) : QObject(parent)
{
    openMIDIPort();
}

void MIDIParser::openMIDIPort()
{
    QString selected_port = promptSelectMIDIPort();

    if(!(selected_port.isEmpty()))
    {
        midi_out.openPort(0, selected_port.toStdString());
    }
}

QString MIDIParser::getMIDIPortFromSettings()
{
    // Try to recall a preferred MIDI port from the application settings.
    QVariant midi_port_identifier = application_settings.value(settings_port_string);
    QString midi_port_name;

    if(!(midi_port_identifier.isNull()))
    {
        midi_port_name = midi_port_identifier.toString();
        bool preferred_port_available = false;

        // Get number of available ports
        int ports_available = midi_out.getPortCount();

        // Check whether the port specified in the application settings is available.
        for(int i = 0; i < ports_available; i++)
        {
            if(midi_port_name == QString::fromStdString(midi_out.getPortName(i)))
            {
                preferred_port_available = true;
            }
        }

        // If the preferred port isn't available, return an empty string.
        if(!preferred_port_available)
        {
            midi_port_name = "";
        }
    }

    return midi_port_name;
}

QString MIDIParser::promptSelectMIDIPort()
{
    QString selected_port;
    int ports_available = midi_out.getPortCount();
    if(ports_available > 0)
    {
        QStringList available_port_names;
        for(int i = 0; i < ports_available; i++)
        {
            available_port_names << QString::fromStdString(midi_out.getPortName(i));
        }

        selected_port = QInputDialog::getItem(0, midi_selector_title, midi_selector_label, available_port_names);
        application_settings.setValue(settings_port_string, selected_port);
        application_settings.sync();
    }

    return selected_port;
}

void MIDIParser::controlBoxData(std::shared_ptr<std::vector<unsigned char>> data)
{
    for(auto byte : *data)
    {
        if(isStatusMessage(byte))
        {
            // A new status message takes precedence over a pending one.
            if(message_buffer.size() > 0)
            {
                message_buffer.clear();
            }

            expected_message_length = getExpectedMessageLength(byte);

            // expected_message_length == 0 when that particular message is unimplemented.
            if(expected_message_length > 0)
            {
                message_buffer.push_back(byte);
            }
        }

        else // (is data message.)
        {
            if(message_buffer.size() > 0)
            {
                message_buffer.push_back(byte);
            }
        }

        if((int)message_buffer.size() == expected_message_length)
        {
            midi_out.sendMessage(&message_buffer);
            message_buffer.clear();
            expected_message_length = 0;
        }
    }
}

void MIDIParser::midiIn(std::shared_ptr<std::vector<unsigned char>> midi_message)
{
    if(midi_out.isPortOpen())
    {
        midi_out.sendMessage(midi_message.get());
    }
}

int MIDIParser::getExpectedMessageLength(unsigned char byte)
{
    byte &= 0xF0; // Clear channel information.
    int expected_message_length = 0;
    switch(byte)
    {
        case 0x80: expected_message_length = 3; break;
        case 0x90: expected_message_length = 3; break;
        case 0xA0: expected_message_length = 3; break;
        case 0xB0: expected_message_length = 3; break;
        case 0xC0: expected_message_length = 2; break;
        case 0xD0: expected_message_length = 2; break;
        case 0xE0: expected_message_length = 3; break;
        default: break;
    }

    return expected_message_length;
}
