#include "visionprocessor.h"

VisionProcessor::VisionProcessor(QObject *parent):
    QObject(parent),
    average_height(0),
    clustersHand(2,2,CV_32S),
    clustersLaser(12,2,CV_32S),
    toggleTrainKmeans(0)
{
    camera.open(1);
    defaultKmeans();
    cv::namedWindow(original_window_name, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(threshold_window_name, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(contours_window_name, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(control_window_name, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(hand_window_name,cv::WINDOW_AUTOSIZE);

    cv::createTrackbar("Train KMeans", control_window_name, &toggleTrainKmeans, 1);
    cv::createTrackbar("Laser Cluster", control_window_name, &correctClusterIndexHand, 12);
    cv::createTrackbar("Erode 1 size", control_window_name, &erode_1_size, 20);
    cv::createTrackbar("Dilate size", control_window_name, &dilate_size, 20);
    cv::createTrackbar("Erode 2 size", control_window_name, &erode_2_size, 20);
    cv::createTrackbar("Blur size", control_window_name, &blur_size, 20);
    cv::createTrackbar("Canny lower threshold", control_window_name, &canny_lower_threshold, 1000);
    cv::createTrackbar("Canny upper threshold", control_window_name, &canny_upper_threshold, 1000);
}

void VisionProcessor::connectMIDIParser(MIDIParser *mp)
{
    midi_parser = mp;
}

void VisionProcessor::getNextFrame()
{
    static cv::Mat frame, frame_edges;

    camera >> frame;
    frame_edges = preprocess(frame);
    cv::findContours(frame_edges, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));
    mean_contours = findMeanContours(contours);

    cv::Mat mean_contours_display = cv::Mat::zeros(frame_edges.size(), CV_8UC1);
    drawOpenContours(mean_contours_display, mean_contours);

    //static cv::VideoWriter raw_writer("raw_output.avi", CV_FOURCC('M','J','P','G'), 30, frame.size(), true);
    //static cv::VideoWriter contours_writer("contours_output.avi", CV_FOURCC('M','J','P','G'), 30, mean_contours_display.size(), false);

    //raw_writer << frame;
    //contours_writer << mean_contours_display;

    if(mean_contours.size() > 0)
    {
        average_height = calculateAverageHeight(mean_contours);
        heightToMIDI(average_height);
        //std::cout << "Average height (cm): " << average_height << std::endl;
    }

    cv::imshow(original_window_name, frame);
    cv::imshow(contours_window_name, mean_contours_display);
    cv::waitKey(1);
}

cv::Mat VisionProcessor::preprocess(const cv::Mat &image)
{
    cv::Mat frame_hls, frame_thresholded, frame_edges, img,imageHand;

    //different clusters and the best cluster index
    //use this instead of the in range function
    //cv::cvtColor(image, frame_thresholded, CV_BGR2HLS);
    cv::cvtColor(image, img, CV_BGR2Lab);
    imageHand = image.clone();
    if (toggleTrainKmeans == 1){
        toggleTrainKmeans = 0;
        trainKmeansFromFrame(img);
    }

    //segment image based on the kmeans parameters
    //trained from matlab
    for(int i = 0; i < img.rows; i++){
        for(int j = 0; j < img.cols; j++){
            int a = img.at<cv::Vec3b>(i,j)[1];
            int b = img.at<cv::Vec3b>(i,j)[2];
            //segment laser
            double compareDistanceLaser = squareDistance(clustersLaser.at<int>(correctClusterIndex,0),clustersLaser.at<int>(correctClusterIndex,1),a,b);

            double compareDistanceHand = squareDistance(clustersLaser.at<int>(correctClusterIndexHand,0),clustersLaser.at<int>(correctClusterIndexHand,1),a,b);

            for (int k = 0;k < 12;k ++){
                if (k != correctClusterIndex){
                    double value = squareDistance(clustersLaser.at<int>(k,0),clustersLaser.at<int>(k,1),a,b);
                    if(value < compareDistanceLaser){
                        img.at<cv::Vec3b>(i,j)[0] = 0;
                        img.at<cv::Vec3b>(i,j)[1] = 0;
                        img.at<cv::Vec3b>(i,j)[2] = 0;
                    }
                    if(value < compareDistanceHand){
                        imageHand.at<cv::Vec3b>(i,j)[0] = 0;
                        imageHand.at<cv::Vec3b>(i,j)[1] = 0;
                        imageHand.at<cv::Vec3b>(i,j)[2] = 0;
                    }
                    if (value < compareDistanceHand && value < compareDistanceLaser){
                        break;
                    }
                }
            }
            //segment hand
//            double compareDistanceHand = squareDistance(clustersHand.at<int>(0,0),clustersHand.at<int>(0,1),a,b);
//            double compareDistanceHandBackground = squareDistance(clustersHand.at<int>(1,0),clustersHand.at<int>(1,1),a,b);
//            if (compareDistanceHand > compareDistanceHandBackground){
//                imageHand.at<cv::Vec3b>(i,j)[0] = 0;
//                imageHand.at<cv::Vec3b>(i,j)[1] = 0;
//                imageHand.at<cv::Vec3b>(i,j)[2] = 0;
//            }
        }
    }

    cv::erode(img, img, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(erode_1_size, erode_1_size)));
    cv::dilate(img, img, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(dilate_size, dilate_size)));
    cv::erode(img, img, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(erode_2_size, erode_2_size)));
    cv::blur(img, img, cv::Size(blur_size, blur_size));
    cv::Canny(img, frame_edges, canny_lower_threshold, canny_upper_threshold, 3);

    cv::imshow(threshold_window_name, img);

    //cv::Point p;
    //find_large_contour_center_of_gravity(imageHand,p);
    //cv::circle(imageHand,p,16,255);
    //cv::imshow(hand_window_name, imageHand);
    trackHand(imageHand);
    cv::imshow(hand_window_name, imageHand);
    //static cv::VideoWriter threshold_writer("threshold_output.avi", CV_FOURCC('M','J','P','G'), 30, frame_thresholded.size(), false);
    //threshold_writer << frame_thresholded;

    return frame_edges;
}

ContourSet VisionProcessor::findMeanContours(ContourSet &contours)
{
    ContourSet mean_contours;
    // For each contour, find the mean vertical point for each image column
    for(auto contour : contours)
    {
        // First, sort the vector by columns then rows
        std::sort(contour.begin(), contour.end(),
                  [] (const cv::Point &i, const cv::Point &j)
                  {
                      return (i.x < j.x) || ((i.x == j.x) && (i.y < j.y));
                  }
                  );

        // Next, take the first and last points of each column and find their midpoint, storing these in a new vector
        mean_contours.emplace_back();
        auto point = contour.begin();
        while(point != contour.end())
        {
            int current_column = point->x;
            int top_row = point->y;
            int bottom_row;

            // Iterate over the points in the current column
            while(point != contour.end() && point->x == current_column)
            {
                bottom_row = point->y;
                point++;
            }

            // Now, find the midpoint between top_row and bottom_row
            double midpoint = ((double)top_row + (double)bottom_row)/2.0;
            int midpoint_pixel = std::round(midpoint);

            // Finally, create a new point with current_column & midpoint_pixel as coordinates
            mean_contours.back().emplace_back(current_column, midpoint_pixel);
        }
    }
    return mean_contours;
}

void VisionProcessor::drawOpenContours(cv::Mat &image, const ContourSet &contours)
{
    for(auto contour : contours)
    {
        for(auto point : contour)
        {
            cv::circle(image, point, 1, 255);
        }
    }
}

void VisionProcessor::defaultKmeans(){
    correctClusterIndex = 11;
    correctClusterIndexHand = 6;
    clustersLaser.at<int>(0,0) = 152;
    clustersLaser.at<int>(0,1) = 118;
    clustersLaser.at<int>(1,0) = 130;
    clustersLaser.at<int>(1,1) = 128;
    clustersLaser.at<int>(2,0) = 151;
    clustersLaser.at<int>(2,1) = 134;
    clustersLaser.at<int>(3,0) = 155;
    clustersLaser.at<int>(3,1) = 141;
    clustersLaser.at<int>(4,0) = 185;
    clustersLaser.at<int>(4,1) = 144;
    clustersLaser.at<int>(5,0) = 164;
    clustersLaser.at<int>(5,1) = 131;
    clustersLaser.at<int>(6,0) = 173;
    clustersLaser.at<int>(6,1) = 148;
    clustersLaser.at<int>(7,0) = 155;
    clustersLaser.at<int>(7,1) = 148;
    clustersLaser.at<int>(8,0) = 149;
    clustersLaser.at<int>(8,1) = 140;
    clustersLaser.at<int>(9,0) = 152;
    clustersLaser.at<int>(9,1) = 145;
    clustersLaser.at<int>(10,0) = 139;
    clustersLaser.at<int>(10,1) = 135;
    clustersLaser.at<int>(11,0) = 203;
    clustersLaser.at<int>(11,1) = 104;
    //ignore the hand thresholds
    //hand thresholds
    clustersHand.at<int>(0,0) = 123;
    clustersHand.at<int>(0,1) = 126;
    clustersHand.at<int>(1,0) = 152;
    clustersHand.at<int>(1,1) = 140;

}

double VisionProcessor::calculateAverageHeight(ContourSet input)
{
    double average_height = 0;
    double point_count = 0;

    double image_angle = 0;
    double total_angle = 0;
    double total_angle_radians = 0;
    double actual_height = 0;

    const static double camera_top_angle = camera_angle + (0.5*camera_fov_angle_vertical);
    const static double image_angle_multiplier = (camera_fov_angle_vertical / image_height_pixels);
    for(auto contour : input)
    {
        for(auto point : contour)
        {
            image_angle = ((double) point.y) * image_angle_multiplier;
            total_angle = camera_top_angle - image_angle;
            total_angle_radians = M_PI*(total_angle/180);
            actual_height = camera_height + (camera_laser_distance * tan(total_angle_radians));
            average_height += actual_height;
            point_count++;
        }
    }

    average_height /= point_count;
    return average_height;
}

void VisionProcessor::heightToMIDI(double height)
{
    static bool is_initialised = false;
    static unsigned char prev_pitch = 0;
    unsigned char pitch = 60 + std::round(height*2);

    if(!is_initialised)
    {
        //midiout.openPort(0);
        std::vector<unsigned char> note_on = {128, pitch, 30};
        std::cout << "Note on " << (int)pitch << std::endl;
        //midiout.sendMessage(&note_on);
        prev_pitch = pitch;
        is_initialised = true;
    }

    else
    {
        if(pitch != prev_pitch)
        {
            std::vector<unsigned char> note_off;
            note_off.push_back(128);
            note_off.push_back(prev_pitch);
            note_off.push_back(30);

            std::vector<unsigned char> note_on;
            note_on.push_back(144);
            note_on.push_back(pitch);
            note_on.push_back(30);

            double control_value = (pitch - 70) * (126)/(120-70);

            std::vector<unsigned char> control_change;
            control_change.push_back(177);
            control_change.push_back(1);
            control_change.push_back((unsigned char)control_value);

            //midiout.sendMessage(&control_change);
            //midiout.sendMessage(&note_off);
            //midiout.sendMessage(&note_on);

            prev_pitch = pitch;
        }
    }
}

void VisionProcessor::trainKmeansFromFrame(cv::Mat frame){
    cv::Mat imgedit;
    cvtColor(frame, imgedit, CV_BGR2Lab);
    clustersLaser = kmeansClusterCenters(imgedit);
}

void VisionProcessor::trackHand(cv::Mat &src)
{
    static bool is_initialised = false;
    static MIDIGenerator top, width, x_pos;
    static int top_midi_val, width_midi_val, x_pos_midi_val;
    if(!is_initialised)
    {
        top.setChannel(1);
        width.setChannel(2);
        x_pos.setChannel(3);
        QObject::connect(&top, SIGNAL(midiOut(std::shared_ptr<std::vector<unsigned char> >)), midi_parser, SLOT(midiIn(std::shared_ptr<std::vector<unsigned char> >)));
        QObject::connect(&width, SIGNAL(midiOut(std::shared_ptr<std::vector<unsigned char> >)), midi_parser, SLOT(midiIn(std::shared_ptr<std::vector<unsigned char> >)));
        QObject::connect(&x_pos, SIGNAL(midiOut(std::shared_ptr<std::vector<unsigned char> >)), midi_parser, SLOT(midiIn(std::shared_ptr<std::vector<unsigned char> >)));
        is_initialised = true;
    }

    std::vector<cv::Point> locations;

    // convert to grayscale
    cv::cvtColor(src, src, CV_BGR2GRAY);

    // remove noise
    cv::erode(src, src, cv::Mat());
    cv::erode(src, src, cv::Mat());
    cv::dilate(src, src, cv::Mat());
    cv::dilate(src, src, cv::Mat());

    // convert to binary image
    cv::threshold(src, src, 0, 255, cv::THRESH_BINARY|cv::THRESH_OTSU);

    //use a bouding box around the hand to decide where position is
    //this could be improved by using a particle tracker with the hand
    //modelled as a particle (x,y,vx,vy,rotation,etc) and feeding these readings
    //into it
    int minHandArea = 256*256;
    int minPointCount = 2500;
    if (cv::countNonZero(src) > minPointCount){
        //std::cout << cv::countNonZero(src);
        cv::findNonZero(src, locations);
        cv::Rect boundingBox = boundingRect(locations);
        //if (boundingBox.width*boundingBox.height > minHandArea){
            std::cout << "\nx:" << boundingBox.x << " y:" << boundingBox.y << " w:" << boundingBox.width << " h:" << boundingBox.height;
            // Generate a midi value from the y coordinate
            top_midi_val = (int)(64.0 + (boundingBox.y/480.0)*(24.0));
            width_midi_val = (int)(64.0 + (boundingBox.width/640.0)*(24.0));
            x_pos_midi_val = (int)(64.0 + ((boundingBox.x + (boundingBox.width/2.0))/480.0)*(24.0));

            top.play(top_midi_val, 63);
            width.play(width_midi_val, 63);
            x_pos.play(x_pos_midi_val, 63);
            rectangle(src,boundingBox.tl(),boundingBox.br(),255,2,8,0);
        //}
    }
}

//provide image in the lab colour space
//this will return the cluster centers
cv::Mat VisionProcessor::kmeansClusterCenters(cv::Mat image){
    int origRows = image.rows;
    cv::Mat clustersLaser(12,2,CV_32S);
    //remove the light component from the image
    cv::Mat channel[3];
    cv::split(image, channel);
    channel[0]= cv::Mat::zeros(image.rows, image.cols, CV_8UC1);//Set light channel to 0
    merge(channel,3,image);

    //convert remaining image into a form that kmeans can use
    cv::Mat colVec = image.reshape(1, image.rows*image.cols); // change to a Nx3 column vector
    cv::Mat colVecD, bestLabels, centers, clustered;
    colVec.convertTo(colVecD, CV_32FC3, 1.0/255.0); // convert to floating point

    //kmeans parameters
    int attempts = 3;
    int clusts = 12;
    double eps = 0.001;
    //cluster
    kmeans(colVecD, clusts, bestLabels,
           cv::TermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, attempts, eps),
           attempts, cv::KMEANS_PP_CENTERS, centers);
    cv::Mat labelsImg = bestLabels.reshape(1, origRows); // single channel image of labels

    //convert kmeans output into a more usable form
    for(int i = 0;i < centers.rows;i ++){
        clustersLaser.at<int>(i,0) = (int)(centers.at<cv::Vec3f>(i)[1]*255);
        clustersLaser.at<int>(i,1) = (int)(centers.at<cv::Vec3f>(i)[2]*255);
    }
    std::cout << clustersLaser;
    return clustersLaser;
}

