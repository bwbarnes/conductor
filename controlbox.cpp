#include "controlbox.h"

ControlBox::ControlBox(QObject *parent) : QObject(parent)
{
    serial_buffer = new char[serial_buffer_length];
    connect(&serial_port, SIGNAL(readyRead()), this, SLOT(onSerialData()));
    openSerialPort();
}

ControlBox::~ControlBox()
{
    delete[] serial_buffer;
}

void ControlBox::onSerialData()
{
    int bytes_read;
    bytes_read = serial_port.read(serial_buffer, serial_buffer_length);

    if(bytes_read != 0)
    {
        // Create a packet to send to the MIDI parser
        std::shared_ptr<std::vector<unsigned char>> packet = std::make_shared<std::vector<unsigned char>>();
        for(int i = 0; i < bytes_read; i++)
        {
            packet->push_back((unsigned char)serial_buffer[i]);
        }
        emit controlBoxMessage(packet);
    }
}

void ControlBox::openSerialPort()
{
    QSerialPortInfo serial_port_info;
    serial_port_info = promptSelectSerialPort();

    // If a port has been successfully selected, open it.
    if(!(serial_port_info.isNull()))
    {
        serial_port.setPort(serial_port_info);
        serial_port.setBaudRate(QSerialPort::Baud9600);
        serial_port.open(QIODevice::ReadWrite);
    }
}

QSerialPortInfo ControlBox::getSerialPortFromSettings()
{
    // Try to recall a preferred serial port from the application settings.
    QVariant serial_port_identifier = application_settings.value(settings_identifier_string);
    QString serial_port_name;

    // Get a list of available serial ports.
    QList<QSerialPortInfo> available_ports = QSerialPortInfo::availablePorts();

    QSerialPortInfo serial_port_info;

    // If a preferred port was found, check whether it is available.
    if(!(serial_port_identifier.isNull()))
    {
        serial_port_name = serial_port_identifier.toString();

        for(int i = 0; i < available_ports.size(); i++)
        {
            if(available_ports.at(i).portName() == serial_port_name)
            {
                // If the preferred port is available, get its information.
                serial_port_info = available_ports.at(i);
                break;
            }
        }
    }

    return serial_port_info;
}

QSerialPortInfo ControlBox::promptSelectSerialPort()
{
    QString serial_port_name;
    QSerialPortInfo serial_port_info;

    // Get a list of available ports
    QList<QSerialPortInfo> available_ports = QSerialPortInfo::availablePorts();

    if(!(available_ports.isEmpty()))
    {
        // Create a list of serial port names
        QStringList available_port_names;
        for(int i  = 0; i < available_ports.size(); i++)
        {
            available_port_names << available_ports.at(i).portName();
        }

        // Create a dialog box allowing the user to select from the available ports.
        serial_port_name = QInputDialog::getItem(0, serial_selector_title, serial_selector_label, available_port_names);
        serial_port_info = QSerialPortInfo(serial_port_name);
    }

    // Write the new selected serial port name to the application settings
    application_settings.setValue(settings_identifier_string, serial_port_name);
    application_settings.sync();

    return serial_port_info;
}
