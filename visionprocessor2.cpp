#include "visionprocessor2.h"

VisionProcessor2::VisionProcessor2(QObject *parent):
	QObject(parent),
    camera(),
	laser_erode_1_size(2),
	laser_dilate_1_size(20),
    laser_erode_2_size(15),
	laser_blur_size(3),
	laser_erode_1(cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(laser_erode_1_size, laser_erode_1_size))),
	laser_dilate_1(cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(laser_dilate_1_size, laser_dilate_1_size))),
    laser_erode_2(cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(laser_erode_2_size, laser_erode_2_size))),
    laser_canny_lower_threshold(100),
    laser_canny_upper_threshold(300),
    hand_min_pixels(2500),
    laser_cluster_index(2),
    hand_cluster_index(0),
    clusters(12,2,CV_32S),
    kmeans_segmentation_space(255, 255, CV_8U)
{
    camera.open(1);
	defaultKmeans();
}


VisionProcessor2::~VisionProcessor2()
{
	//
}

void VisionProcessor2::getNextFrame()
{
	// Get next frame of video
	cv::Mat frame;
	camera >> frame;

	// Segment into laser and hand images
	cv::Mat laser, hand;
	segment(frame, laser, hand);

    cv::imshow("Original", frame);

    // Get bounding box from hand image
    cv::Rect hand_bounds;
    bool hand_present = getHandBounds(hand, hand_bounds);

    // Get average height from laser image
    double average_height;
    average_height = getAverageHeight(laser);
}

void VisionProcessor2::defaultKmeans(){
    clusters.at<int>(0,0) = 184;
    clusters.at<int>(0,1) = 145;
    clusters.at<int>(1,0) = 170;
    clusters.at<int>(1,1) = 143;
    clusters.at<int>(2,0) = 197;
    clusters.at<int>(2,1) = 108;
    clusters.at<int>(3,0) = 129;
    clusters.at<int>(3,1) = 127;
    clusters.at<int>(4,0) = 147;
    clusters.at<int>(4,1) = 140;
    clusters.at<int>(5,0) = 92;
    clusters.at<int>(5,1) = 139;
    clusters.at<int>(6,0) = 152;
    clusters.at<int>(6,1) = 137;
    clusters.at<int>(7,0) = 151;
    clusters.at<int>(7,1) = 129;
    clusters.at<int>(8,0) = 146;
    clusters.at<int>(8,1) = 120;
    clusters.at<int>(9,0) = 152;
    clusters.at<int>(9,1) = 143;
    clusters.at<int>(10,0) = 159;
    clusters.at<int>(10,1) = 125;
    clusters.at<int>(11,0) = 146;
    clusters.at<int>(11,1) = 134;
    computeKmeansABLabelsForAllPixels();
}


void VisionProcessor2::computeKmeansABLabelsForAllPixels()
{
    for(int i = 0; i < kmeans_segmentation_space.rows; i++){
        for(int j = 0; j < kmeans_segmentation_space.cols; j++){
            int a = i;
            int b = j;
            int minDistance = -1;
            int minK = 0;
            for (int k = 0;k < 12;k ++){
                int compareWith = squareDistance(clusters.at<int>(k,0),clusters.at<int>(k,1),a,b);
                if (compareWith <= minDistance || minDistance < 0){
                    minK = k;
                    minDistance = compareWith;
                }
            }

            kmeans_segmentation_space.at<uchar>(i,j) = minK;

//            kmeans_segmentation_space.at<uchar>(i,j) = LABEL_BACKGROUND;
//            if (minK == laser_cluster_index){
//                kmeans_segmentation_space.at<uchar>(i,j) = LABEL_LASER;
//            }
//            if (minK == hand_cluster_index){
//                kmeans_segmentation_space.at<uchar>(i,j) = LABEL_HAND;
//            }
        }
    }
}

void VisionProcessor2::segment(const cv::Mat &source, cv::Mat &laser, cv::Mat &hand)
{
	// Convert source to LAB
	cv::Mat source_lab;
	cv::cvtColor(source, source_lab, CV_BGR2Lab);

	// Create deep copies of the source image for the laser and hand results
	laser = source.clone();
	hand = source.clone();

	// Iterate over source image, clearing any pixels in the laser and hand images which do not belong
    for(int row_index = 0; row_index < source_lab.rows; row_index++)
	{
        for(int col_index = 0; col_index < source_lab.cols; col_index++)
		{
			// Get 'a' and 'b' values of the current pixel
			int a = source_lab.at<cv::Vec3b>(row_index, col_index)[1];
            int b = source_lab.at<cv::Vec3b>(row_index, col_index)[2];

            // Check whether these 'a' and 'b' values match the laser or hand, clear pixel if not
            if(!isHand(a, b))
            {
            	hand.at<cv::Vec3b>(row_index, col_index)[0] = 0;
            	hand.at<cv::Vec3b>(row_index, col_index)[1] = 0;
            	hand.at<cv::Vec3b>(row_index, col_index)[2] = 0;
            }

            if(!isLaser(a, b))
            {
            	laser.at<cv::Vec3b>(row_index, col_index)[0] = 0;
            	laser.at<cv::Vec3b>(row_index, col_index)[1] = 0;
            	laser.at<cv::Vec3b>(row_index, col_index)[2] = 0;
            }
		}
	}
}

bool VisionProcessor2::getHandBounds(const cv::Mat &source, cv::Rect &bounds)
{
	// Create and preprocess a working copy of the hand image
	cv::Mat hand;
	preprocessHand(source, hand);

	// Check whether sufficient 'hand' pixels are present
	if(cv::countNonZero(hand) < hand_min_pixels)
	{
		return false;
	}

	// Create bounding box for 'hand' pixels
	std::vector<cv::Point> pix_locations;
	cv::findNonZero(hand, pix_locations);
    bounds = boundingRect(pix_locations);
	return true;

}

double VisionProcessor2::getAverageHeight(const cv::Mat &input)
{
	cv::Mat laser;
	preprocessLaser(input, laser);
    return 0;
}

void VisionProcessor2::preprocessHand(const cv::Mat &source, cv::Mat &hand_processed)
{
	// Convert image to greyscale
	cv::cvtColor(source, hand_processed, CV_BGR2GRAY);

    cv::erode(hand_processed, hand_processed, laser_erode_1);
    cv::imshow("hand_processed", hand_processed);

    // convert to binary image
    cv::threshold(hand_processed, hand_processed, 0, 255, cv::THRESH_BINARY|cv::THRESH_OTSU);
}

void VisionProcessor2::preprocessLaser(const cv::Mat &source, cv::Mat &laser_processed)
{
    // Create a working image
    cv::Mat laser_working;

	// Convert image to greyscale
    cv::cvtColor(source, laser_working, CV_BGR2GRAY);

    cv::erode(laser_working, laser_working, laser_erode_1);
    cv::dilate(laser_working, laser_working, laser_dilate_1);
    cv::erode(laser_working, laser_working, laser_erode_2);
    cv::imshow("laser_working", laser_working);

    // Perform edge detection
    cv::blur(laser_working, laser_working, cv::Size(laser_blur_size, laser_blur_size));
    cv::Canny(laser_working, laser_processed, laser_canny_lower_threshold, laser_canny_upper_threshold, 3);
}
