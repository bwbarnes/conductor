#include "midigenerator.h"

MIDIGenerator::MIDIGenerator(QObject *parent):
    QObject(parent),
    note_on(false)
{

}

MIDIGenerator::~MIDIGenerator()
{
    stopNote();
}

void MIDIGenerator::setChannel(uint8_t channel_number)
{
    stopNote(); // Ensures current note isn't locked on.
    current_note.channel = channel_number;
}

void MIDIGenerator::play(uint8_t note_number, uint8_t velocity)
{
    stopNote();
    uint8_t status_byte = 0x90 + current_note.channel;
    current_note.note = note_number;
    current_note.velocity = velocity;
    std::shared_ptr<std::vector<unsigned char>> midi_message = std::make_shared<std::vector<unsigned char>>();
    midi_message->push_back(status_byte);
    midi_message->push_back(note_number);
    midi_message->push_back(velocity);
    emit midiOut(midi_message);
    note_on = true;
}



void MIDIGenerator::stopNote()
{
    if(note_on)
    {
        uint8_t status_byte = 0x80 + current_note.channel;
        std::shared_ptr<std::vector<unsigned char>> midi_message = std::make_shared<std::vector<unsigned char>>();
        midi_message->push_back(status_byte);
        midi_message->push_back(current_note.note);
        midi_message->push_back(current_note.velocity);
        emit midiOut(midi_message);
        note_on = false;
    }
}

