#ifndef MIDITESTER_H
#define MIDITESTER_H

#include <QObject>

const uint8_t notes[] = {
    64,
    64,
    76,
    64,
    74,
    64,
    74,
    76
};

class MIDITester : public QObject
{
    Q_OBJECT
public:
    explicit MIDITester(QObject *parent = 0);

signals:
    void noteOut(uint8_t note, uint8_t velocity);

public slots:
    void next();

private:
    int index, chord;
};

#endif // MIDITESTER_H
