#include "controlbox.h"
#include "mainwindow.h"
#include "midigenerator.h"
#include "midiparser.h"
#include "miditester.h"
#include "visionprocessor2.h"

#include <QApplication>
#include <QObject>
#include <QSettings>
#include <QTimer>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Team Titan");
    QCoreApplication::setApplicationName("Conductor");

    ControlBox control_box;
    MIDIParser midi_parser;
    QObject::connect(&control_box, SIGNAL(controlBoxMessage(std::shared_ptr<std::vector<unsigned char> >)), &midi_parser, SLOT(controlBoxData(std::shared_ptr<std::vector<unsigned char> >)));

    MainWindow w;
    w.show();

    VisionProcessor2 vision_processor(0);

    QTimer frame_timer;
    QObject::connect(&frame_timer, SIGNAL(timeout()), &vision_processor, SLOT(getNextFrame()));
    frame_timer.start(100);

    return a.exec();
}
