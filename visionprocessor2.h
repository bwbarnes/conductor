#ifndef VISIONPROCESSOR2_H
#define VISIONPROCESSOR2_H

// Qt includes
#include <QObject>

// Library includes
#include <opencv2/opencv.hpp>

// Typedefs
typedef std::vector<cv::Point> Contour;
typedef std::vector<Contour> ContourSet;

// Defines
#define LABEL_BACKGROUND 0
#define LABEL_LASER 128
#define LABEL_HAND 254

class VisionProcessor2 : public QObject
{
	    Q_OBJECT

public:
    explicit VisionProcessor2(QObject *parent = 0);
            ~VisionProcessor2();

public slots:
	void getNextFrame();

private:
    cv::VideoCapture camera;

    int laser_erode_1_size;
    int laser_dilate_1_size;
    int laser_erode_2_size;
    int laser_blur_size;

	cv::Mat laser_erode_1;
	cv::Mat laser_dilate_1;
	cv::Mat laser_erode_2;

    int laser_canny_lower_threshold;
    int laser_canny_upper_threshold;

    int hand_min_pixels;
    int laser_cluster_index;
    int hand_cluster_index;
    cv::Mat clusters;
    cv::Mat kmeans_segmentation_space;

	// Main k-means based segmentation function
    void defaultKmeans();
	void computeKmeansABLabelsForAllPixels();
	void segment(const cv::Mat &source, cv::Mat &laser, cv::Mat &hand);

	// These functions recover usable data from the segmented images
    bool getHandBounds(const cv::Mat &source, cv::Rect &bounds);
    double getAverageHeight(const cv::Mat &input);

	// These functions perform all necessary preprocessing on the images
	void preprocessHand(const cv::Mat &source, cv::Mat &hand_processed);
	void preprocessLaser(const cv::Mat &source, cv::Mat &laser_processed);

	// These functions classify a pixel as hand, laser or background based on colour
    inline bool isLaser(int a, int b)
	{
        return kmeans_segmentation_space.at<uchar>(a,b) == laser_cluster_index;
	}

    inline bool isHand(int a, int b)
	{
        return kmeans_segmentation_space.at<uchar>(a,b) == hand_cluster_index;
	}

    // Used to generate the kmeans_segmentation_space
    inline double squareDistance(int a1, int b1, int a2, int b2){
        return (a1-a2)*(a1-a2) + (b1-b2)*(b1-b2);
    }

};

#endif // VISIONPROCESSOR2_H
