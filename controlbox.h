#ifndef CONTROLBOX_H
#define CONTROLBOX_H

// STL includes
#include <memory>

// Qt includes
#include <QInputDialog>
#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QSettings>
#include <QString>

// Persistent settings entries
const QString settings_identifier_string("serial/identifier");

// Serial selector settings
const QString serial_selector_title("Select serial port");
const QString serial_selector_label("Please select the control box's serial port from the list below.");

// Serial parser settings
const int serial_buffer_length = 64;

class ControlBox : public QObject
{
    Q_OBJECT
public:
    explicit ControlBox(QObject *parent = 0);
    ~ControlBox();

signals:
    void controlBoxMessage(std::shared_ptr<std::vector<unsigned char>> packet);
public slots:
    void onSerialData();

private:
    // Private member variables
    QSettings   application_settings;
    QSerialPort serial_port;
    char *serial_buffer;

    // Private member functions
    void openSerialPort();
    QSerialPortInfo getSerialPortFromSettings();
    QSerialPortInfo promptSelectSerialPort();
};

#endif // CONTROLBOX_H
