#include "miditester.h"

MIDITester::MIDITester(QObject *parent):
    QObject(parent),
    index(0),
    chord(0)
{

}

void MIDITester::next()
{
    static int counter = 0;
    emit noteOut(notes[index] - chord, 63);
    index++;
    if(index > 7)
    {
        index = 0;
        counter++;
    }

    if(counter == 4)
    {
        counter = 0;
        if(chord == 0)
        {
            chord = 2;
        }
        else
        {
            chord = 0;
        }
    }
}

