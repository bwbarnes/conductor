#ifndef MIDIPARSER_H
#define MIDIPARSER_H

// STL includes
#include <cstdint>
#include <memory>
#include <vector>

// Qt includes
#include <QInputDialog>
#include <QObject>
#include <QSettings>
#include <QString>

// Library includes
#include <RtMidi.h>

// Persistent settings entries
const QString settings_port_string = "midi/port";

// MIDI port selector settings
const QString midi_selector_title = "Select MIDI Port";
const QString midi_selector_label = "Please select a MIDI output port from the list below.";

class MIDIParser : public QObject
{
    Q_OBJECT
public:
    explicit MIDIParser(QObject *parent = 0);
    void openMIDIPort();
    QString getMIDIPortFromSettings();
    QString promptSelectMIDIPort();

signals:

public slots:
    void controlBoxData(std::shared_ptr<std::vector<unsigned char>> data);
    void midiIn(std::shared_ptr<std::vector<unsigned char>> midi_message);

private:
    // Private member variables
    RtMidiOut midi_out;

    // Message parsing
    std::vector<unsigned char> message_buffer;
    int expected_message_length;

    // Application
    QSettings application_settings;

    // Private member functions
    inline bool isStatusMessage(const unsigned char &byte)
    {
        return (byte & 0x80);
    }

    int getExpectedMessageLength(unsigned char byte);
};

#endif // MIDIPARSER_H
