#ifndef VISIONPROCESSOR_H
#define VISIONPROCESSOR_H

#include <QObject>

#include "midigenerator.h"
#include "midiparser.h"

#include <opencv2/opencv.hpp>

typedef std::vector<cv::Point> Contour;
typedef std::vector<Contour> ContourSet;

const std::string original_window_name = "Original";
const std::string hand_window_name = "Hand";
const std::string threshold_window_name = "Threshold";
const std::string contours_window_name = "Contours";
const std::string control_window_name = "Controls";

class VisionProcessor : public QObject
{
    Q_OBJECT
public:
    explicit VisionProcessor(QObject *parent = 0);
    void connectMIDIParser(MIDIParser *mp);

signals:

public slots:
    void getNextFrame();

private:
    MIDIParser *midi_parser;
    // Image processing
    cv::VideoCapture camera;
    ContourSet contours, mean_contours;
    std::vector<cv::Vec4i> hierarchy;
    int h_min = 150;
    int h_max = 179;
    int l_min = 150;
    int l_max = 255;
    int s_min = 200;
    int s_max = 255;

    int erode_1_size = 2;
    int erode_2_size = 15;
    int dilate_size = 20;
    int blur_size = 3;
    int canny_lower_threshold = 100;
    int canny_upper_threshold = 300;

    // K-means
    int toggleTrainKmeans;
    int correctClusterIndex = 2;
    int correctClusterIndexHand = 11;
    cv::Mat clustersHand;
    cv::Mat clustersLaser;

    // Height
    double average_height;

    // Geometry
    double camera_angle = 45.0; // Degrees
    double camera_laser_distance = 12.0; // Centimetres
    double camera_height = 8; // Centimetres
    double camera_fov_angle_vertical = 30; // Degrees
    double image_height_pixels = 480;

    void defaultKmeans();
    cv::Mat preprocess(const cv::Mat &image);
    ContourSet findMeanContours(ContourSet &contours);
    void drawOpenContours(cv::Mat &image, const ContourSet &contours);
    double calculateAverageHeight(ContourSet input);
    void heightToMIDI(double height);
    void trainKmeansFromFrame(cv::Mat frame);
    void trackHand(cv::Mat &src);
    cv::Mat kmeansClusterCenters(cv::Mat image);

    inline double squareDistance(int a1, int b1, int a2, int b2){
        return (a1-a2)*(a1-a2) + (b1-b2)*(b1-b2);
    }
};

#endif // VISIONPROCESSOR_H
