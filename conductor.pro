#-------------------------------------------------
#
# Project created by QtCreator 2015-05-18T08:08:35
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = conductor
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    controlbox.cpp \
    midiparser.cpp \
    midigenerator.cpp \
    miditester.cpp \
    visionprocessor2.cpp

HEADERS  += mainwindow.h \
    controlbox.h \
    midiparser.h \
    midigenerator.h \
    miditester.h \
    visionprocessor2.h

FORMS    += mainwindow.ui

DISTFILES +=

unix|win32: LIBS += -lrtmidi
unix|win32: LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc
