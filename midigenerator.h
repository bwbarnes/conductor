#ifndef MIDIGENERATOR_H
#define MIDIGENERATOR_H

// STL includes
#include <memory>

#include <QObject>

typedef struct
{
    uint8_t channel;
    uint8_t note;
    uint8_t velocity;
} MIDINote;

class MIDIGenerator : public QObject
{
    Q_OBJECT
public:
    explicit MIDIGenerator(QObject *parent = 0);
    ~MIDIGenerator();

signals:
    void midiOut(std::shared_ptr<std::vector<unsigned char>> midi_message);

public slots:
    void setChannel(uint8_t channel_number);
    void play(uint8_t note_number, uint8_t velocity);
    void stopNote();

private:
    bool note_on;
    MIDINote current_note;
};

#endif // MIDIGENERATOR_H
